import React, { useState } from "react";
import moment from "moment";
import Numeral from "numeral";
// Material UI
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";
import Slide from "@material-ui/core/Slide";
import IconButton from "@material-ui/core/IconButton";
// Icons
import DeleteIcon from "@material-ui/icons/Close";
// Assets
import logo from "./logo.png";
import "./App.css";

const App = ({ classes }) => {
  const [loading, setLoading] = useState(false);
  const [view, setView] = useState(0);
  const [prices, setPrices] = useState([]);
  const [toDate, setToDate] = useState(moment().format("YYYY-MM-DD"));
  const [fromDate, setFromDate] = useState(
    moment()
      .subtract(5, "months")
      .format("YYYY-MM-DD")
  );

  const fetchPrices = async () => {
    setLoading(true);
    setView(1);
    const response = await fetch(
      `https://api.coindesk.com/v1/bpi/historical/close.json?start=${fromDate}&end=${toDate}`
    );
    const data = await response.json();
    const bpi = Object.entries(data.bpi);
    const selectedDays = bpi.filter(([date, price]) =>
      isPricePrime(Math.floor(price))
    );
    setPrices(selectedDays);
    setLoading(false);
  };

  const isPricePrime = price => {
    const primesInPrice = [];
    for (let i = 2; i <= price; i++) {
      if (isNumberPrime(i) === true) {
        primesInPrice.push(i);
      }
    }
    return isNumberPrime(primesInPrice.length);
  };

  const isNumberPrime = price => {
    if (price < 2) return false;
    for (let i = 2, s = Math.sqrt(price); i <= s; i++) {
      if (price % i === 0) return false;
    }
    return true;
  };

  const maxDate = moment(fromDate)
    .add(5, "months")
    .isBefore(moment())
    ? moment(fromDate)
        .add(5, "months")
        .format("YYYY-MM-DD")
    : moment().format("YYYY-MM-DD");

  return (
    <div className="App">
      <div className={classes.foreground}>
        <Grid container className={classes.root}>
          <Grid item md={12} xs={12}>
            <img className="logo" src={logo} alt="logo" />
          </Grid>
          <Grid item md={12} xs={12}>
            <Typography variant="h5" component="h3">
              Find the Prime Days for the Selected Date Range!
            </Typography>
            <Typography component="p">
              Looks through each day's Bitcoin price and filters for days where
              the total number of prime numbers, contained in the price, is a
              prime number itself.
            </Typography>
            <Divider className={classes.divider} />

            <Slide direction="up" in={view === 0} mountOnEnter unmountOnExit>
              <form className={classes.container} noValidate>
                <div className={classes.inline}>
                  <TextField
                    label="Select Price From"
                    type="date"
                    value={fromDate}
                    className={classes.textField}
                    onChange={event => setFromDate(event.target.value)}
                    inputProps={{ max: moment().format("YYYY-MM-DD") }}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                  <TextField
                    label="Select Prices To"
                    type="date"
                    value={toDate}
                    className={classes.textField}
                    onChange={event => setToDate(event.target.value)}
                    inputProps={{ max: maxDate, min: fromDate }}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </div>
                <Button
                  color="primary"
                  variant="contained"
                  className={classes.button}
                  onClick={fetchPrices}
                >
                  View Prices
                </Button>
              </form>
            </Slide>

            <Slide direction="up" in={view === 1} mountOnEnter unmountOnExit>
              {loading ? (
                <CircularProgress className={classes.progress} />
              ) : prices ? (
                <div className={classes.tableBody}>
                  <div className={classes.tableContainer}>
                    <Table className={classes.table}>
                      <TableHead>
                        <TableRow>
                          <TableCell>Date</TableCell>
                          <TableCell align="right">Price</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody classes={{ root: classes.tableBody }}>
                        {prices.map(([date, price]) => (
                          <TableRow key={date}>
                            <TableCell component="th" scope="row">
                              {date}
                            </TableCell>
                            <TableCell align="right">
                              <strong>
                                {Numeral(price).format("$0,0.00")}
                              </strong>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </div>
                  <IconButton
                    onClick={() => setView(0)}
                    className={classes.iconButton}
                    aria-label="Delete"
                  >
                    <DeleteIcon className={classes.icon} />
                  </IconButton>
                </div>
              ) : (
                <Typography component="p">
                  Please select date range to fetch prices
                </Typography>
              )}
            </Slide>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

const styles = theme => ({
  button: {
    backgroundColor: "#ef6c00",
    "&:hover": {
      backgroundColor: "#fb8c00"
    }
  },
  divider: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    backgroundColor: "#ffcc80",
    height: "2px"
  },
  icon: {
    color: "#fff"
  },
  iconButton: {
    backgroundColor: "#ef6c00",
    "&:hover": {
      backgroundColor: "#fb8c00"
    },
    position: "fixed",
    bottom: "15px"
  },
  inline: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3
  },
  progress: {
    color: "#ef6c00"
  },
  tableBody: {
    maxHeight: "70vh",
    overflowY: "auto"
  },
  tableContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  table: {
    maxWidth: "50vw"
  },
  textField: {
    margin: theme.spacing.unit,
    width: 200
  },
  foreground: {
    zIndex: 20
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

export default withStyles(styles)(App);
